﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpodIglyMVC.Controllers;
using System.Web.Mvc;
using SpodIglyMVC.ViewModels;
using System.Linq;
using SpodIglyMVC.Models;
using System.Collections.Generic;
using Moq;
using System.Data.Entity;
using SpodIglyMVC.DAL;
using SpodIglyMVC.Infrastructure;

namespace SpodIglyMVC.Tests
{
    [TestClass]
    public class HomeControllerTests
    {
        // Mocking Entity Framework and checking view model
        [TestMethod]
        public void IndexAction_Returns3BestsellersAnd3Newitems()
        {
            // Arrange
            var data = new List<Album> 
            { 
                new Album { IsBestseller = true, IsHidden = false }, 
                new Album { IsBestseller = true, IsHidden = false }, 
                new Album { IsBestseller = true, IsHidden = false }, 
                new Album { IsBestseller = false, IsHidden = false }, 
                new Album { IsBestseller = false, IsHidden = false }, 
                new Album { IsBestseller = true, IsHidden = true}, 
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Album>>();
            mockSet.As<IQueryable<Album>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Album>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Album>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Album>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<StoreContext>();
            mockContext.Setup(c => c.Albums).Returns(mockSet.Object);

            var mockCache = new Mock<ICacheProvider>();

            var controller = new HomeController(mockContext.Object, mockCache.Object);

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            var viewModel = result.ViewData.Model as HomeViewModel;
            Assert.IsTrue(viewModel.Bestsellers.Count() == 3);
            Assert.IsTrue(viewModel.NewArrivals.Count() == 3);
        }

        // Checking View name
        [TestMethod]
        public void StaticContentAction_WithViewNamePassed_ReturnsViewWithTheSameName()
        {
            // Arrange
            var mockContext = new Mock<StoreContext>();
            var mockCache = new Mock<ICacheProvider>();
            var controller = new HomeController(mockContext.Object, mockCache.Object);

            // Act
            var result = controller.StaticContent("TestView") as ViewResult;

            // Assert
            Assert.AreEqual("TestView", result.ViewName);
        }

        // Integration test (not a real unit test) - uses actual db
        //[TestMethod]
        //public void IndexAction_Returns3BestsellersAnd3Newitems()
        //{
        //    // Arrange
        //    var controller = new HomeController();

        //    // Act
        //    var result = controller.Index() as ViewResult;

        //    // Assert
        //    var viewModel = result.ViewData.Model as HomeViewModel;
        //    Assert.IsTrue(viewModel.Bestsellers.Count() == 3);
        //    Assert.IsTrue(viewModel.NewArrivals.Count() == 3);
        //}

        //[TestMethod]
        //public void StaticContentAction_WithViewNamePassed_ReturnsViewWithTheSameName()
        //{
        //    // Arrange
        //    var controller = new HomeController();

        //    // Act
        //    var result = controller.StaticContent("TestView") as ViewResult;

        //    // Assert
        //    Assert.AreEqual("TestView", result.ViewName);
        //}
    }
}
