﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpodIglyMVC.Controllers;
using SpodIglyMVC.DAL;
using SpodIglyMVC.Infrastructure;
using SpodIglyMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SpodIglyMVC.Tests
{
    [TestClass]
    public class ManageControllerTests
    {
        // Testing RedirectResult
        [TestMethod]
        public async Task ChangePassword_WithModelErrors_RedirectsToIndex()
        {
            // Arrange
            var userStore = new Mock<IUserStore<ApplicationUser>>();
            var userManager = new ApplicationUserManager(userStore.Object);
            var controller = new ManageController(userManager);
            var vm = new ChangePasswordViewModel();
            controller.ModelState.AddModelError("key", "error");

            // Act
            var result = (RedirectToRouteResult)await controller.ChangePassword(vm);

            // Assert
            Assert.AreEqual("Index", result.RouteValues["Action"]);
        }

        // Mocking our own IMailService
        [TestMethod]
        public void ChangeOrderState_OrderstateChangesToShipped_SendsConfirmationEmail()
        {
            // Arrange            
            var mockSet = new Mock<DbSet<Order>>();            
            Order orderToModify = new Order { OrderId = 1, OrderState = OrderState.New };
            mockSet.Setup(m => m.Find(It.IsAny<int>())).Returns(orderToModify);

            var mockContext = new Mock<StoreContext>();
            mockContext.Setup(c => c.Orders).Returns(mockSet.Object);            

            // Mock MailService
            var mailMock = new Mock<IMailService>();

            Order orderArgument = new Order { OrderId = 1, OrderState = OrderState.Shipped };            

            var controller = new ManageController(mockContext.Object, mailMock.Object);

            // Act
            var result = controller.ChangeOrderState(orderArgument);

            // Assert
            mailMock.Verify(m => m.SendOrderShippedEmail(It.IsAny<Order>()), Times.Once());
        }
    }
}
