﻿using SpodIglyMVC.DAL;
using SpodIglyMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SpodIglyMVC.Controllers
{
	public class StoreController : Controller
	{
		StoreContext db = new StoreContext();

		//
		// GET: /Store/
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Details(int id)
		{
			var album = db.Albums.Find(id);
			
			return View(album);
		}

		public ActionResult List(string genrename, string searchQuery = null)
		{
			string decodedGenre = HttpUtility.UrlDecode(genrename);

			var genre = db.Genres.Include("Albums").Where(g => g.Name.ToUpper() == decodedGenre.ToUpper()).Single();
			var albums = genre.Albums.Where(a => (searchQuery == null || 
                                                a.AlbumTitle.ToLower().Contains(searchQuery.ToLower()) ||
                                                a.ArtistName.ToLower().Contains(searchQuery.ToLower())) &&
                                                !a.IsHidden);

            if (Request.IsAjaxRequest())
            {
                return PartialView("_ProductList", albums);
            }


            return View(albums);
		}

        public ActionResult AlbumsSuggestions(string term)
        {
            var albums = this.db.Albums.Where(a => a.AlbumTitle.ToLower().Contains(term.ToLower()) && !a.IsHidden).Take(5).Select(a => new { label = a.AlbumTitle });
          
            return Json(albums, JsonRequestBehavior.AllowGet);
        }

		[ChildActionOnly]
        [OutputCache(Duration = 86400)]
		public ActionResult GenresMenu()
		{
			var genres = db.Genres;

			return PartialView(genres);
		}

        protected override void Dispose(bool disposing)
        {
            if (this.db != null)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}