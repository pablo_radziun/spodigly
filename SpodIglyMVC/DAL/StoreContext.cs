﻿using Microsoft.AspNet.Identity.EntityFramework;
using SpodIglyMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SpodIglyMVC.DAL
{
    public class StoreContext : IdentityDbContext<ApplicationUser>
    {
        public StoreContext()
            : base("StoreContext")
        {
            
        }

        static StoreContext()
        {
            Database.SetInitializer<StoreContext>(new StoreInitializer());
        }

        public virtual DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

    //        modelBuilder.Entity<ApplicationUser>().HasMany(a => a.Orders).WithRequired().WillCascadeOnDelete(true);
            modelBuilder.Entity<Order>().HasRequired(o => o.User);
            

    //.HasForeignKey(p => p.DepartmentId)
    //.WillCascadeOnDelete(false);


            // Change the name of the table to be Users instead of AspNetUsers
            //modelBuilder.Entity<IdentityUser>()
            //    .ToTable("Users");
            //modelBuilder.Entity<ApplicationUser>()
            //    .ToTable("Users");
}


            
        public static StoreContext Create()
        {
            return new StoreContext();
        }
    }
}